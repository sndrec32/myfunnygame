using UnrealBuildTool;

public class MyFunnyGameTarget : TargetRules
{
	public MyFunnyGameTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("MyFunnyGame");
	}
}
